package com.canary.eslam.newsfeedsapp.enities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Article(

	@field:SerializedName("publishedAt")
	val publishedAt: String? = null,

	@field:SerializedName("author")
	val author: String? = null,

	@field:SerializedName("urlToImage")
	val urlToImage: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("url")
	val url: String? = null
) : Parcelable {
	constructor(parcel : Parcel) : this(
			parcel.readString() ,
			parcel.readString() ,
			parcel.readString() ,
			parcel.readString() ,
			parcel.readString() ,
			parcel.readString())

	override fun writeToParcel(parcel : Parcel , flags : Int) {
		parcel.writeString(publishedAt)
		parcel.writeString(author)
		parcel.writeString(urlToImage)
		parcel.writeString(description)
		parcel.writeString(title)
		parcel.writeString(url)
	}

	override fun describeContents() : Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Article> {
		override fun createFromParcel(parcel : Parcel) : Article {
			return Article(parcel)
		}

		override fun newArray(size : Int) : Array<Article?> {
			return arrayOfNulls(size)
		}
	}
}