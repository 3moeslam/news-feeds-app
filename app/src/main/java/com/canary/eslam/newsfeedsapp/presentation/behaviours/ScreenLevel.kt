package com.canary.eslam.newsfeedsapp.presentation.behaviours

import androidx.annotation.StringRes

/**
 * Indication to first level screen.
 */
interface FirstLevelScreen{
	/**
	 * Return Screen name which will appear in actionbar
	 */
	fun getScreenNameID():Int
}

/**
 * Indication to second level screen.
 */
interface SecondLevelScreen{
	/**
	 * Return Screen name which will appear in actionbar
	 */
	fun getScreenNameID():Int
}

/**
 * Default implementation of first level screen interface
 */
class FirstLevelScreenImpl (@StringRes private val screenNameID:Int):FirstLevelScreen{
	override fun getScreenNameID() : Int = screenNameID
}

/**
 * Default implementation of second level screen interface
 */
class SecondLevelScreenImpl (@StringRes private val screenNameID:Int):SecondLevelScreen{
	override fun getScreenNameID() : Int = screenNameID

}