package com.canary.eslam.newsfeedsapp.presentation.behaviours

import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.navigation.Navigation

fun navigator(view : View? , @IdRes id : Int , bundle : Bundle) {
	if (view != null)
		Navigation.findNavController(view).navigate(id , bundle)
}