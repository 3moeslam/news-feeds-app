package com.canary.eslam.newsfeedsapp.presentation.extensions

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun View.hide() {
	visibility = View.GONE
}

fun View.show() {
	visibility = View.VISIBLE
}

fun ImageView.loadImage(url:String?){
	Glide.with(this)
			.load(url)
			.centerCrop()
			.fitCenter()
			.into(this)
}