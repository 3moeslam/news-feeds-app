# News Feeds App
Android Application displays list of articles

## Design 
Application Design philosophy taken from ( Clean Architecture By Robert Marten ).

Design contain 5 Layers

from outer circle to inner circle
1. DI ( Dependency injection ) outer circle
2. Frameworks ( Network )
3. Presentation ( Application class, Activities , fragments )
4. Domain ( Gatewayes to Connect with other layers ,  UseCases )
5. Entities ( Only Pojos )


### Techinical Specs for each layer

#### DI ( Dependency injection )
The Most popular DI sdks in Android is Dagger, i think it is not better choices for the following reasons
1. Complex (In implementation)
2. Depend on annotation processor, so it increase compile time.
3. poor error indicator when compile fail.

In other hand we have [Koin](https://insert-koin.io/ "Koin Home") DI sdk  with no reflection, no code generation.
Koin built by kotlin and taget kotlin at first, but they support java too.

In this Layer i will used Koin.

To Add Item to be injectable
1. open  `KoinModule`
2. Inside `module { ... }` you can add anything you need to inject
    - use `factory { ... }` to inject new instance each time.
    - use `single { ... }` to keep unique instance.
    - use `viewModel { ... }` to create injectable inctance for view  model
3. To Inject Instance (not viewmodel) inside Any Class use inject method it return lazy object
for ex:
`    val api = inject<Api> ()
`
4. To Inject ViewModel
`
val viewModel = viewModel<HomeViewModel>()
`
I think by using koin i decrease Complexty of implementation process and compilation time.

#### Frameworks (Network)
Include all necessary classes to build Domain Network Gateway Interface, it use Retrofit (https://square.github.io/retrofit/)
and okhttp3 (http://square.github.io/okhttp/).
Output : Implementation of Domain layer Network Gateway interface.


#### Presentation
Application Presentation layer Application Based on [Single activity architecture](https://www.youtube.com/watch?v=2k8x8V77CrU "Single activity architecture") recommened by google,
So main screen is fragments, architecture of each screen based on MVVM architectural pattern [Guide to app architecture](https://developer.android.com/jetpack/docs/guide "jetpack guide")

##### 1- articleslist
Include all classes nessesarly to display list of articles like 

*__ArticlesFragment:__* subclass from androidx Fragment to display recycler view

*__ArticlesViewModel:__* subclass from androidx ViewModel to hold all ArticlesFragment states and invoke all logic required to load data

*__ArticlesAdapter:__* include two classes 

1- ArticlesAdapter That's used by recycler view to display list of articles

2- ArticleItem That's used by ArticlesAdapter as view holder

##### 2- article

*__ArticleFragment:__* subclass from androidx Fragment to display single article 

There are no needing to viewmodel or any architecture because it do nothing except display incoming pojo

##### 3- behaviours
*__Navigator:__* Simple class contain navigate method that's user Navigator class to navigate between screens, 
logic created in this way to be able to test navigation logic unit tests using robolectric(https://robolectric.org/) , also to increase abstraction level in application design

*__ScreenLevel:__* Include two interfaces 

1- FirstLevelScreen: used to represent first level screen

2- FirstLevelScreenImpl: default implementation of FirstLevelScreen interface
 
3- SecondLevelScreen: used to represent second level screen
  
4- SecondLevelScreenImpl: default implementation of SecondLevelScreen interface

##### 4- extensions
Include all android extension functions to make code more readable 


##### 5- Classes
*__1- App:__* application class, now it just start koin
*__2- MainActivity:__* application single activity, it manage screen levels, side navigation search logic

 

#### Domain
All usecase used in application by method signature to reach higher level in abstraction
so it can be replaced with changing method name in di without anything else,
also to achieve SRP (single responsibility principle),
and it will be helpful in unit testing by mocking it easily.

