package com.canary.eslam.newsfeedsapp.domain.gateways

import com.canary.eslam.newsfeedsapp.enities.AssociatedPress
import com.canary.eslam.newsfeedsapp.enities.NextWeb
import io.reactivex.Single
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface NewsFeedApi {
	@GET("articles?source=associated-press")
	fun loadAssociatedPress() : Single<AssociatedPress>

	@GET("articles?source=the-next-web")
	fun loadNextWeb() : Single<NextWeb>

}