package com.canary.eslam.newsfeedsapp.di

import android.os.Bundle
import android.view.View
import com.canary.eslam.newsfeedsapp.domain.dateFormater
import com.canary.eslam.newsfeedsapp.presentation.articleslist.ArticlesViewModel
import com.canary.eslam.newsfeedsapp.presentation.behaviours.navigator
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val koinModule = module {
	viewModel { ArticlesViewModel() }
	factory<(View? , Int , Bundle)->Unit> { ::navigator }
	factory<(String?)->String> { ::dateFormater }
}