package com.canary.eslam.newsfeedsapp.enities

import com.google.gson.annotations.SerializedName

data class NextWeb(

		@SerializedName("sortBy")
		val sortBy : String? = null ,

		@SerializedName("source")
		val source : String? = null ,

		@SerializedName("articles")
		val articles : List<Article>? = null ,

		@SerializedName("status")
		val status : String? = null
)