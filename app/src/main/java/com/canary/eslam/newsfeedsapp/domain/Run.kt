package com.canary.eslam.newsfeedsapp.domain

class Result<out T>(
		val value : T? = null ,
		val error : Throwable? = null
)

inline fun <R> runDangerous(block : () -> R) : Result<R> {
	return try {
		val value = block()
		Result(value = value)
	} catch (e : Throwable) {
		Result(error = e)
	}
}