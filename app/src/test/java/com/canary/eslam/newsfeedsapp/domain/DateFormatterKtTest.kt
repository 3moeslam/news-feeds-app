package com.canary.eslam.newsfeedsapp.domain

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DateFormatterKtTest{

	@Test
	fun `with 22_2_2019 should return February 22, 2019`(){
		val result = dateFormater("2019-02-22T09:09:53Z")
		assertEquals("February 22, 2019",result)
	}

	@Test
	fun `with 21_2_2019 should return February 21, 2019`(){
		val result = dateFormater("2019-01-21T09:09:53Z")
		assertEquals("January 21, 2019",result)
	}
}