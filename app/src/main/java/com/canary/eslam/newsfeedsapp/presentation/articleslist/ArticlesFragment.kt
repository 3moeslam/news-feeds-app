package com.canary.eslam.newsfeedsapp.presentation.articleslist

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.canary.eslam.newsfeedsapp.R
import com.canary.eslam.newsfeedsapp.enities.Article
import com.canary.eslam.newsfeedsapp.presentation.behaviours.FirstLevelScreen
import com.canary.eslam.newsfeedsapp.presentation.behaviours.FirstLevelScreenImpl
import com.canary.eslam.newsfeedsapp.presentation.extensions.hide
import com.canary.eslam.newsfeedsapp.presentation.extensions.show
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Articles Screen - sub-class of {@link Fragment}
 * This screen display list of articles
 */
class ArticlesFragment : Fragment() ,
		FirstLevelScreen by FirstLevelScreenImpl(R.string.link_development) {

	private val viewModel : ArticlesViewModel by viewModel()
	private val navigator : (View? , Int , Bundle) -> Unit by inject()

	override fun onCreateView(inflater : LayoutInflater , container : ViewGroup? ,
							  savedInstanceState : Bundle?) : View? {
		return inflater.inflate(R.layout.fragment_home , container , false)
	}

	/**
	 * When view created
	 *
	 * observe view model livedata
	 * init views actions
	 */
	override fun onViewCreated(view : View , savedInstanceState : Bundle?) {
		with(viewModel){
			loadingStatus.observe(this@ArticlesFragment , onLoadingChanged())
			dialogMessageID.observe(this@ArticlesFragment , onDialogRequeseted())
			articles.observe(this@ArticlesFragment , onArticlesReady())
		}

		try_again_btn.setOnClickListener { viewModel.reloadArticles() }
	}


	/**
	 * Called When loading changed
	 * 	if loadingFinished
	 * 	  hide loading indecator
	 * 	  show articles list
	 * 	else
	 * 	  show loading indecator
	 * 	  hide articles list
	 * 	  hide error view
	 *
	 */
	private fun onLoadingChanged() : Observer<Boolean> = Observer { loadingFinished ->
		if (loadingFinished) {
			loading_indecator.hide()
			articles_list.show()
		} else {
			loading_indecator.show()
			articles_list.hide()
			error_layout.hide()
		}
	}

	/**
	 * Create Dialog when dialog requested
	 */
	private fun onDialogRequeseted() : Observer<Int> = Observer { contentStringID ->
		if (contentStringID != null)
			AlertDialog.Builder(context)
					.setMessage(getString(contentStringID))
					.setPositiveButton(getString(R.string.try_again)) { _ , _ -> viewModel.reloadArticles().also { error_layout.hide() } }
					.setNegativeButton(getString(R.string.cancel)) { dialog , _ -> dialog.dismiss().also { error_layout.show() } }
					.create()
					.show()
	}

	/**
	 * When articles list not null
	 * 	1- Hide error layout
	 * 	2- populate articles
	 * or When articles null
	 *  show error view
	 */
	private fun onArticlesReady() : Observer<List<Article>> = Observer { articleList ->
		if (articleList != null) {
			error_layout.hide()

			articles_list.apply {
				adapter = ArticlesAdapter(list = articleList , onItemSelected = ::onItemSelected)
				layoutManager = LinearLayoutManager(context)
			}
		} else {
			error_layout.show()
		}
	}

	/**
	 * Navigate to article fragment with selected article
	 */
	private fun onItemSelected(article : Article) {
		navigator(view , R.id.article_fragment , bundleOf("article" to article))
	}

}
