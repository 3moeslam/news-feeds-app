package com.canary.eslam.newsfeedsapp.domain

import com.canary.eslam.newsfeedsapp.domain.gateways.NewsFeedApi
import com.canary.eslam.newsfeedsapp.enities.Article
import com.canary.eslam.newsfeedsapp.enities.AssociatedPress
import com.canary.eslam.newsfeedsapp.enities.NextWeb
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verifySequence
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.jetbrains.annotations.TestOnly
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ArticleLoaderTest {

	val mockedApi : NewsFeedApi = mockk()
	private val scheduler = TestScheduler()
	@BeforeEach
	fun clear() {
		clearMocks(mockedApi)
	}

	@Test
	fun `when load should invoke 2 apis`() {
		every { mockedApi.loadNextWeb() } returns Single.just(NextWeb())
		every { mockedApi.loadAssociatedPress() } returns Single.just(AssociatedPress())

		loadArticles(api = mockedApi)
				.subscribeOn(scheduler)
				.test()
		scheduler.triggerActions()

		verifySequence {
			mockedApi.loadNextWeb()
			mockedApi.loadAssociatedPress()
		}
	}

	@Test
	fun `when load should combine 2 apis`() {
		val article1 = Article()
		val article2 = Article()
		val nextWebArticles = listOf(article1)
		val associatedPressArticles = listOf(article2)

		every { mockedApi.loadNextWeb() } returns Single.just(NextWeb(articles = nextWebArticles))
		every { mockedApi.loadAssociatedPress() } returns Single.just(AssociatedPress(articles = associatedPressArticles))

		val testObserver = loadArticles(api = mockedApi)
				.subscribeOn(scheduler)
				.test()

		scheduler.triggerActions()

		testObserver.assertValue {
			it.contains(article1) && it.contains(article2)
		}
	}


}

object Resources {

	@TestOnly
	inline fun <reified T> loadList(path : String) : List<T>? {
		return javaClass.classLoader
				?.getResource(path)
				?.let { File(it.path) }
				?.let { it.readText() }
				?.let { Gson().fromJson<List<T>>(it , object : TypeToken<List<T>>() {}.type) }
	}
}