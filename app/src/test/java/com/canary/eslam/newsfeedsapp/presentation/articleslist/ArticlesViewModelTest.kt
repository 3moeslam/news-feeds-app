package com.canary.eslam.newsfeedsapp.presentation.articleslist

import com.canary.eslam.newsfeedsapp.InstantExecutorExtension
import com.canary.eslam.newsfeedsapp.R
import com.canary.eslam.newsfeedsapp.domain.gateways.NewsFeedApi
import com.canary.eslam.newsfeedsapp.enities.Article
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantExecutorExtension::class)
internal class ArticlesViewModelTest {


	private val api = mockk<NewsFeedApi>()
	private val articlesLoader = mockk<(NewsFeedApi) -> Single<List<Article>>>()
	private val scheduler = TestScheduler()

	@Nested
	inner class InitTest {


		@Test
		fun `init must emmit false and invoke articlesLoader`() {
			every { articlesLoader(api) } returns Single.just(listOf())

			val viewModel = ArticlesViewModel(api , articlesLoader , scheduler)

			verify { articlesLoader(api) }
			assertFalse(viewModel.loadingStatus.value!!)
		}


		@Test
		fun `with single error should emmit loading true and dialog error id`() {
			every { articlesLoader(api) } returns Single.error(Throwable())

			val viewModel = ArticlesViewModel(api , articlesLoader , scheduler)
			scheduler.triggerActions()


			verify { articlesLoader(api) }
			assertTrue(viewModel.loadingStatus.value!!)

			assertEquals(R.string.app_name , viewModel.dialogMessageID.value)
		}

		@Test
		fun `with single success should emmit loading true and list`() {
			val list = listOf<Article>()
			every { articlesLoader(api) } returns Single.just(list)

			val viewModel = ArticlesViewModel(api , articlesLoader , scheduler)
			scheduler.triggerActions()


			verify { articlesLoader(api) }
			assertTrue(viewModel.loadingStatus.value!!)

			assertNull(viewModel.dialogMessageID.value)
			assertEquals(list , viewModel.articles.value)
		}

	}

	@Nested
	inner class ReloadTest {

		private val viewModel : ArticlesViewModel = ArticlesViewModel(api , articlesLoader , scheduler , false)

		@Test
		fun `reload must emmit false and invoke articlesLoader`() {
			every { articlesLoader(api) } returns Single.just(listOf())

			viewModel.reloadArticles()


			verify { articlesLoader(api) }
			assertFalse(viewModel.loadingStatus.value!!)
		}


		@Test
		fun `with single error should emmit loading true and dialog error id`() {
			every { articlesLoader(api) } returns Single.error(Throwable())

			viewModel.reloadArticles()
			scheduler.triggerActions()


			verify { articlesLoader(api) }
			assertTrue(viewModel.loadingStatus.value!!)

			assertEquals(R.string.app_name , viewModel.dialogMessageID.value)
		}

		@Test
		fun `with single success should emmit loading true and list`() {
			val list = listOf<Article>()
			every { articlesLoader(api) } returns Single.just(list)

			viewModel.reloadArticles()
			scheduler.triggerActions()


			verify { articlesLoader(api) }
			assertTrue(viewModel.loadingStatus.value!!)

			assertNull(viewModel.dialogMessageID.value)
			assertEquals(list , viewModel.articles.value)
		}

	}
}