package com.canary.eslam.newsfeedsapp.presentation

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation
import com.canary.eslam.newsfeedsapp.R
import com.canary.eslam.newsfeedsapp.presentation.behaviours.FirstLevelScreen
import com.canary.eslam.newsfeedsapp.presentation.behaviours.SecondLevelScreen
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener {

	private lateinit var currentFragment : Fragment
	private var toggle : ActionBarDrawerToggle? = null

	override fun onCreate(savedInstanceState : Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		setSupportActionBar(toolbar)

		initializeNavigationView()
		addScreenLevelsSupport()
	}


	override fun onCreateOptionsMenu(menu : Menu?) : Boolean {
		menuInflater.inflate(R.menu.main , menu)
		return true
	}

	/**
	 * handle each screen levels by adding fragment lifecycle listener
	 * to check fragment if implements {@link FirstLevelScreen} or {@link SecondLevelScreen}
	 */
	private fun addScreenLevelsSupport() {
		supportFragmentManager.registerFragmentLifecycleCallbacks(fragmentLifecycleCallbacks , true)
	}

	/**
	 * Fragment lifecycle listener Which will implement screen level logic
	 * By default fragment work as First level screen with title R.string.link_development
	 */
	private val fragmentLifecycleCallbacks = object : FragmentManager.FragmentLifecycleCallbacks() {
		override fun onFragmentStarted(fm : FragmentManager , f : Fragment) {
			when (f) {
				is SecondLevelScreen -> changeTitle(f.getScreenNameID()).also { showBack() }
				is FirstLevelScreen -> changeTitle(f.getScreenNameID()).also { hideBack() }
				else -> changeTitle(R.string.link_development).also { hideBack() }
			}
			currentFragment = f
		}
	}

	/**
	 * Initialize toggle (Burger menu)
	 * Add navigation items selection listener
	 */
	private fun initializeNavigationView() {
		toggle = ActionBarDrawerToggle(
				this ,
				drawer_layout ,
				toolbar ,
				R.string.navigation_drawer_open ,
				R.string.navigation_drawer_close
		)

		drawer_layout.addDrawerListener(toggle!!)
		toggle!!.syncState()
		toggle!!.setHomeAsUpIndicator(R.drawable.ic_back)

		nav_view.setNavigationItemSelectedListener(this)
	}

	/**
	 * Handle Back pressing
	 */
	override fun onBackPressed() {
		if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
			drawer_layout.closeDrawer(GravityCompat.START)
		} else {
			super.onBackPressed()
		}
	}


	/**
	 * Handle navigation view item clicks.
	 */
	override fun onNavigationItemSelected(item : MenuItem) : Boolean {
		when (item.itemId) {
			R.id.action_explore -> {
				showToast(R.string.explore_txt)
			}
			R.id.action_live_chat -> {
				showToast(R.string.live_chat_txt)
			}
			R.id.action_gallery -> {
				showToast(R.string.gallery_txt)
			}
			R.id.action_wish_list -> {
				showToast(R.string.wish_list_txt)
			}
			R.id.action_e_magazine -> {
				showToast(R.string.e_magazine_txt)
			}
		}

		drawer_layout.closeDrawer(GravityCompat.START)
		return true
	}

	/**
	 * Show toast with activity context and short period
	 *
	 * @param messageID message String res id
	 */
	private fun showToast(@StringRes messageID : Int) {
		Toast.makeText(this , messageID , Toast.LENGTH_SHORT).show()
	}

	/**
	 * Hide toolbar back button
	 */
	fun hideBack() {
		drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
		toggle?.isDrawerIndicatorEnabled = true
	}

	/**
	 * Show toolbar back button
	 */
	fun showBack() {
		drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
		toggle?.isDrawerIndicatorEnabled = false
		toggle?.setToolbarNavigationClickListener {
			Navigation.findNavController(this , R.id.fragment).popBackStack()
		}
	}

	/**
	 * change activty title
	 *
	 * @param titleID  title String res id
	 */
	fun changeTitle(titleID : Int) {
		supportActionBar?.title = getString(titleID)
	}
}
