package com.canary.eslam.newsfeedsapp.frameworks

import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.net.InetAddress


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class NetworkBuilderKtTest {
	private val server = MockWebServer()
	private val scheduler = TestScheduler()

	@BeforeAll
	fun startServer() {
		server.start(InetAddress.getByName("localhost") , 3399)
		println("Server url : http://${server.hostName}:${server.port}/")
	}


	@Test
	fun `any request should contain apiKey`() {
		testNewsFeedApi.loadNextWeb()
				.subscribeOn(Schedulers.io())
				.subscribe()

		val request = server.takeRequest()

		assertThat(request.requestUrl.query()).contains("apiKey").contains("533af958594143758318137469b41ba9")
	}

	@AfterAll
	fun closeServer() {
		server.close()
	}

}