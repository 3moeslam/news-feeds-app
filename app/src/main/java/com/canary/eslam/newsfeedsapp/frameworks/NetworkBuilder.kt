package com.canary.eslam.newsfeedsapp.frameworks

import com.canary.eslam.newsfeedsapp.domain.gateways.NewsFeedApi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val baseUrl = "https://newsapi.org/v1/"
const val baseTestUrl = "http://localhost:3399/"
val okHttpClient by lazy<OkHttpClient> {
	OkHttpClient.Builder()
			.addInterceptor(headerInterceptor)
			.addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
			.connectTimeout(30 , TimeUnit.SECONDS)
			.readTimeout(30 , TimeUnit.SECONDS)
			.build()
}


val headerInterceptor by lazy {
	Interceptor { chain ->
		var request = chain.request()

		val url = request.url()
				.newBuilder()
				.addQueryParameter("apiKey" , "533af958594143758318137469b41ba9")
				.build()

		request = request.newBuilder()
				.url(url)
				.build()

		chain.proceed(request)
	}
}
val newsFeedApi by lazy<NewsFeedApi> {
	Retrofit.Builder()
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.baseUrl(baseUrl)
			.client(okHttpClient)
			.build()
			.create(NewsFeedApi::class.java)
}
val testNewsFeedApi by lazy<NewsFeedApi> {
	Retrofit.Builder()
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.baseUrl(baseTestUrl)
			.client(okHttpClient)
			.build()
			.create(NewsFeedApi::class.java)
}