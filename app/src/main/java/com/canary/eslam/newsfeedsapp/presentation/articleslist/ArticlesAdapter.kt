package com.canary.eslam.newsfeedsapp.presentation.articleslist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.canary.eslam.newsfeedsapp.R
import com.canary.eslam.newsfeedsapp.domain.dateFormater
import com.canary.eslam.newsfeedsapp.enities.Article
import com.canary.eslam.newsfeedsapp.presentation.extensions.loadImage
import kotlinx.android.synthetic.main.article_item.view.*


class ArticlesAdapter(
		private val list : List<Article> ,
		private val dateFormatter : (String?) -> String = ::dateFormater ,
		private val onItemSelected : (Article) -> Unit
) : RecyclerView.Adapter<ArticleItem>() {

	override fun onCreateViewHolder(parent : ViewGroup , viewType : Int) : ArticleItem = parent.context
			.let { LayoutInflater.from(it) }
			.let { it.inflate(R.layout.article_item , parent , false) }
			.let { ArticleItem(it) }


	override fun getItemCount() : Int = list.size


	@SuppressLint("SetTextI18n")
	override fun onBindViewHolder(holder : ArticleItem , position : Int) = list[position].run {
		holder.article_title.text = title
		holder.bt_text.text = "By $author"
		val date = dateFormatter(publishedAt)
		holder.date_txt.text = date
		holder.article_image.loadImage(urlToImage)
		holder.article_container.setOnClickListener { onItemSelected(this) }
	}


}

class ArticleItem(view : View) : RecyclerView.ViewHolder(view) {
	val article_image = view.article_image!!
	val article_title = view.article_title!!
	val bt_text = view.bt_text!!
	val date_txt = view.date_txt!!
	val article_container = view.article_container!!
}