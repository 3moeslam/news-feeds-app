package com.canary.eslam.newsfeedsapp.domain

import com.canary.eslam.newsfeedsapp.domain.gateways.NewsFeedApi
import com.canary.eslam.newsfeedsapp.enities.Article
import com.canary.eslam.newsfeedsapp.enities.AssociatedPress
import com.canary.eslam.newsfeedsapp.enities.NextWeb
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith

/**
 * Method to load Articles by combining next web response with associated press response
 * {@see Requirement 3}
 *
 * @param api Implementation of {@link NewsFeedApi}
 *
 * @return Single of list of combined articles
 */
fun loadArticles(api : NewsFeedApi) : Single<List<Article>> {
	return api.loadNextWeb()
			.zipWith(api.loadAssociatedPress() , ::articlesZipper)
}

/**
 * Method combine two response lists
 *
 */
private fun articlesZipper(nextWeb : NextWeb , associatedPress : AssociatedPress) : MutableList<Article> {
	val nextWebArticles = nextWeb.articles
	val associatedPressArticles = associatedPress.articles

	return mutableListOf<Article>()
			.apply { nextWebArticles?.let { addAll(it) } }
			.apply { associatedPressArticles?.let { addAll(it) } }
}