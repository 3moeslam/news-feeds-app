package com.canary.eslam.newsfeedsapp.presentation.article


import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.canary.eslam.newsfeedsapp.R
import com.canary.eslam.newsfeedsapp.enities.Article
import com.canary.eslam.newsfeedsapp.presentation.behaviours.SecondLevelScreen
import com.canary.eslam.newsfeedsapp.presentation.behaviours.SecondLevelScreenImpl
import com.canary.eslam.newsfeedsapp.presentation.extensions.loadImage
import kotlinx.android.synthetic.main.fragment_article.*
import org.koin.android.ext.android.inject


class ArticleFragment : Fragment() ,
		SecondLevelScreen by SecondLevelScreenImpl(R.string.link_development) {

	private val dateFormatter : (String?) -> String by inject()

	override fun onCreateView(inflater : LayoutInflater , container : ViewGroup? ,
							  savedInstanceState : Bundle?) : View? {
		return inflater.inflate(R.layout.fragment_article , container , false)
	}

	override fun onViewCreated(view : View , savedInstanceState : Bundle?) {
		arguments?.getParcelable<Article>("article")
				?.run { bindArticle(this) }
	}

	@SuppressLint("SetTextI18n")
	private fun bindArticle(article : Article) {
		with(article) {
			article_image.loadImage(urlToImage)
			title_txt.text = title
			auther_txt.text = "By $author"
			content_txt.text = description
			date_txt.text = dateFormatter(publishedAt)
			open_website.setOnClickListener { openWebsite(url) }
		}
	}

	private fun openWebsite(url : String?) {
		Intent(Intent.ACTION_VIEW).apply {
			data = Uri.parse(url)
			startActivity(this)
		}
	}

}
