package com.canary.eslam.newsfeedsapp.domain

import com.canary.eslam.newsfeedsapp.domain.runDangerous
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.lang.RuntimeException


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RunDangerousTest {
	@Test
	fun `with success should return value and null error`() {
		val result = runDangerous { "value" }

		assertEquals("value" , result.value)
		assertNull(result.error)
	}

	@Test
	fun `with throwable should return null value and throwable in error`(){
		val result = runDangerous { throw RuntimeException() }

		assertTrue(result.error is RuntimeException)
		assertNull(result.value)
	}
}