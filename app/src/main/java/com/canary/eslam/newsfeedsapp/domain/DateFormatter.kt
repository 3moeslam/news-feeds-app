package com.canary.eslam.newsfeedsapp.domain

import java.text.SimpleDateFormat

fun dateFormater(date : String?) : String =
		SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
				.let { it.parse(date) }
				.let { SimpleDateFormat("MMMM dd, yyyy").format(it) }
