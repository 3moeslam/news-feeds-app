package com.canary.eslam.newsfeedsapp.presentation.articleslist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.canary.eslam.newsfeedsapp.R
import com.canary.eslam.newsfeedsapp.domain.gateways.NewsFeedApi
import com.canary.eslam.newsfeedsapp.domain.loadArticles
import com.canary.eslam.newsfeedsapp.enities.Article
import com.canary.eslam.newsfeedsapp.frameworks.newsFeedApi
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Articles screen view model,
 * Hold articles screen data, and invoke business logic
 *
 * @param api Implementation of {@link NewsFeedApi}, by default it refer to production implementation
 * 			  you can inject mock in testing
 * @param articlesLoader Method take {@link NewsFeedApi} and return {@link Single} of {@link List} of {@link Article}
 * 						 by default it refer to production implementation you can inject mock in testing
 * @param scheduler schedulers to control rxJava threads, by default it refer to io scheduler,
 * 					Note: all data sent to UI must be posted on LiveData, subscriber will receive data in main thread
 *
 * @param loadArticlesWhenStart boolean to load articles at initiating view model , by default true to load at starting
 */
class ArticlesViewModel(
		private val api : NewsFeedApi = newsFeedApi ,
		private val articlesLoader : (NewsFeedApi) -> Single<List<Article>> = ::loadArticles ,
		private val scheduler : Scheduler = Schedulers.io() ,
		loadArticlesWhenStart : Boolean = true
) : ViewModel() {

	/**
	 * Hold articles loading status, true if loaded
	 */
	val loadingStatus : MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
	/**
	 * Hold articles list.
	 */
	val articles : MutableLiveData<List<Article>> by lazy { MutableLiveData<List<Article>>() }
	/**
	 * Hold string id of dialog message.
	 */
	val dialogMessageID : MutableLiveData<Int> by lazy { MutableLiveData<Int>() }


	private val disposables = CompositeDisposable()

	init {
		if (loadArticlesWhenStart)
			loadArticlesList()
	}

	/**
	 * reload articles from server.
	 */
	fun reloadArticles() {
		articles.postValue(null)
		dialogMessageID.postValue(null)

		loadArticlesList()
	}

	/**
	 * Subscribe to articles list single
	 * and add disposable to CompositeDisposable
	 * and reset loading status
	 */
	private fun loadArticlesList() {
		loadingStatus.postValue(false)

		val d = articlesLoader(api)
				.subscribeOn(scheduler)
				.observeOn(scheduler)
				.subscribe(::onSuccess , ::onFail)
		disposables.add(d)
	}

	/**
	 * on articles loaded successfully
	 */
	private fun onSuccess(list : List<Article>) {
		articles.postValue(list)
		loadingStatus.postValue(true)
	}

	/**
	 * on articles loading fail
	 */
	private fun onFail(throwable : Throwable) {
		dialogMessageID.postValue(R.string.app_name)
		loadingStatus.postValue(true)
	}

	/**
	 * On Clear view model
	 */
	override fun onCleared() {
		disposables.clear()
		super.onCleared()
	}
}