package com.canary.eslam.newsfeedsapp.enities

import com.google.gson.annotations.SerializedName

data class AssociatedPress(

		@field:SerializedName("sortBy")
		val sortBy : String? = null ,

		@field:SerializedName("source")
		val source : String? = null ,

		@field:SerializedName("articles")
		val articles : List<Article>? = null ,

		@field:SerializedName("status")
		val status : String? = null
)