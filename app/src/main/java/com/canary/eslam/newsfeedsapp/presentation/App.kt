package com.canary.eslam.newsfeedsapp.presentation

import android.app.Application
import com.canary.eslam.newsfeedsapp.di.koinModule
import org.koin.android.ext.android.startKoin

class App:Application(){
	override fun onCreate() {
		super.onCreate()

		startKoin(this , listOf(koinModule))
	}
}

